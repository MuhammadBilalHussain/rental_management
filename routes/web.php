<?php

use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\GeneralController;
use App\Http\Controllers\Admin\PermissionController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\frontend\FrontendController;
use App\Http\Controllers\frontend\OrderController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [FrontendController::class,'index']);
Route::get('/product-detail/{id}',[\App\Http\Controllers\frontend\ProductController::class,'productDetail']);
Route::post('/book-room', [OrderController::class,'checkout']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['auth']], function () {
    Route::get('/categories', [CategoryController::class, 'index'])->name('categoryList')->middleware('can:categories');
    Route::post('/store-category', [CategoryController::class, 'store'])->name('storeCategory');
    Route::post('edit-category/{id}', [CategoryController::class, 'editcategory']);
    Route::delete('delete-category/{id}', [CategoryController::class, 'deletecategory']);

    Route::get('/sub-categories', [CategoryController::class, 'subcategoryList'])->middleware('can:sub-categories');
    Route::post('/add-sub-categories', [CategoryController::class, 'addsubcategory'])->name('storesubCategory');
    Route::post('/edit-sub-category/{id}', [CategoryController::class, 'Updatesubcategory']);
    Route::delete('delete-sub-category/{id}', [CategoryController::class, 'deleteSubcategory']);

    // role routes
    Route::get('roles', [RoleController::class, 'roles'])->middleware('can:roles');
    Route::get('add-role', [RoleController::class, 'Addrole']);
    Route::post('save-role', [RoleController::class, 'SaveRole'])->name('storeRole');
    Route::get('edit-role/{id}', [RoleController::class, 'EditRole'])->name('editRole')->middleware('can:edit-role');
    Route::post('update-role/{id}', [RoleController::class, 'Update'])->name('UpdateRole');
    Route::delete('delete-role/{id}', [RoleController::class, 'deleteRole']);


    // permissions route
    Route::get('permissions', [PermissionController::class, 'permissions'])->middleware('can:permissions');
    Route::post('/store-permissions', [PermissionController::class, 'storePermissions'])->name('storePermissions');
    Route::post('edit-permission/{id}', [PermissionController::class, 'editpermission']);
    Route::delete('delete-permissions/{id}', [PermissionController::class, 'Deletepermission']);
    // users routes

    Route::get('users', [GeneralController::class, 'users'])->middleware('can:users');
    Route::post('edit-user-profile/{id}', [GeneralController::class, 'EditUser']);
    Route::delete('delete-user/{id}', [GeneralController::class, 'DeleteUser']);




    Route::get('profile', [UserController::class, 'userProfile']);
    Route::post('update-profile', [UserController::class, 'ProfileUpdate'])->name('UpdateProfile');
    Route::post('update-user-location', [UserController::class, 'LocationUpdate'])->name('UpdateUserLocation');




    //
    Route::get('products', [ProductController::class, 'index'])->middleware('can:products');
    Route::get('add-product', [ProductController::class, 'create']);
    Route::post('save-product', [ProductController::class, 'store']);


    //
    Route::get('icons', [GeneralController::class, 'icons'])->name('icons')->middleware('can:icons');
});
