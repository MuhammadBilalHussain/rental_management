<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\ProductFacility;
use App\Models\ProductFeature;
use App\Models\ProductImage;
use App\Models\ProductOffer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    public function index(){
        $products = Product::with(['ProductCategory'])->get();
        return view('admin.products.index',compact('products'));
    }
    public function create(){
        $categories = Category::all();
        return view('admin.products.create',compact('categories'));
    }
    public function store(Request $request){
        $arr = [
            'category_id'=>$request->category,
            'name'=>$request->product_name,
            'price'=>$request->price,
            'discounted_price'=>$request->discounted_price,
            'short_description'=>$request->short_description,
            'long_description'=>$request->long_description,
            'added_by'=>Auth::id()
        ];
        if ($request->hasFile('main_image')) {
            $mainImage = $request->file('main_image');
            $imageName = time() . rand(10, 999) . '.' . $mainImage->getClientOriginalExtension();
            $mainImage->move(public_path('product'), $imageName);
            $arr['product_image'] = 'product/'.$imageName;
        }

        if ($request->hasFile('video')) {
            $video = $request->file('video');
            $imageName = time() . rand(10, 999) . '.' . $video->getClientOriginalExtension();
            $video->move(public_path('product'), $imageName);
            $arr['video'] = 'product/'.$imageName;
        }

            $product = Product::create($arr);

        foreach ($request->other_images as $image){
            $imageName = time(). rand(10,999) . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('product'), $imageName);
            ProductImage::create([
                'product_id'=> $product->id,
                'image'=> 'product/'.$imageName
            ]);
        }

        foreach ($request->facilities as $facility) {
            ProductFacility::create([
                "product_id"=>$product->id,
                "icon" => $facility['icon'],
                "details" => $facility['details']
            ]);
        }
        foreach ($request->features as $feature) {
            ProductFeature::create([
                "product_id"=>$product->id,
                "icon" => $feature['icon'],
                "details" => $feature['details'],
                "description" => $feature['description']
            ]);
        }
        foreach ($request->offer as $offer) {
            ProductOffer::create([
                "product_id"=>$product->id,
                "icon" => $offer['icon'],
                "details" => $offer['details']
            ]);
        }

        return back();
    }
}
