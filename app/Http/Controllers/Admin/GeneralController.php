<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserProfile;
use Illuminate\Http\Request;

class GeneralController extends Controller
{
    public function icons()
    {
        return view('admin.icons');
    }



    public function users()
    {
        $users = User::with('UserProfile')->where('id', '!=', 1)->get();
        // dd($users);
        return view('admin.user-management.users.users', compact('users'));
    }

    public function EditUser(Request $request, $id)
    {
        // dd($request);


        UserProfile::updateOrCreate([
            'user_id' => $id
        ], [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone_no' => $request->phone_no,
        ]);
        return back()->with(['success' => 'User updated successfully.']);
    }

    public function DeleteUser($id)
    {
        // dd($id);
        User::find($id)->delete();
        return back()->with(['success' => 'User deleted successfully.']);
    }
}
