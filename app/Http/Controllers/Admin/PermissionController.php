<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    //

    public function permissions()
    {
        $permissions = Permission::all();
        return view('admin.user-management.permissions.permissions', compact('permissions'));
    }
    public function storePermissions(Request $request){
        $validatedData = $request->validate(
            [
                'name' => 'required',
                'guard' => 'required',

            ],
            [
                'name.required' => 'Permission name is required',
                'guard.required' => 'Guard name is required',

            ]
        );
        Permission::create([
            'name' => $request->name,
            'guard_name' => $request->guard,


        ]);
        return back()->with(['success' => 'Permission added successfully.']);
    }

    public function editpermission(Request $request, $id)
    {

        // dd($id);
        $validatedData = $request->validate(
            [
                'name' => 'required',
                'guard' => 'required',

            ],
            [
                'name.required' => 'Permission name is required',
                'guard.required' => 'Guard name is required',

            ]
        );
        Permission::where('id', $id)->update([
            'name' => $request->name,
            'guard_name' => $request->guard,
        ]);
        return back()->with(['success' => 'Permission updated successfully.']);
    }

    public function Deletepermission(Request $request, $id)
    {
        //  dd($id);

        Permission::find($id)->delete();
        return back()->with(['success' => 'Permission deleted successfully.']);
    }
}
