<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    //

    public function roles(){
        $roles = Role::all();
        return view('admin.user-management.role.roles', compact('roles'));
    }

    public function Addrole(){
        $permissions = Permission::all();
        return view('admin.user-management.role.add-role', compact('permissions'));

    }


    public function SaveRole(Request $request){
        //   dd($request);
        $validatedData = $request->validate(
            [
                'name' => 'required',
                'guard_name' => 'required',

            ],
            [
                'name.required' => 'Role name is required',
                'guard_name.required' => 'Guard name is required',

            ]
        );
        $role = Role::create([
            'name' => $request->name,
            'guard_name' => $request->guard_name,
            
        ]);
        $role->syncPermissions($request->permissions);
        return redirect()->route('admin.roles')->with(['success' => 'Role added successfully.']);

    }

    public function EditRole($id)
    {

        $role = Role::find($id);
        $permissions = Permission::all();
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id", $id)
            ->pluck('role_has_permissions.permission_id', 'role_has_permissions.permission_id')
            ->all();
        return view('admin.user-management.role.edit-role', compact('role', 'permissions', 'rolePermissions'));
    }


    public function Update(Request $request, $id)
    {

        // dd($request);
        $validatedData = $request->validate(
            [
                'name' => 'required',
            ],
            [
                'name.required' => 'Role name is required',
            ]
        );
        $role = Role::find($id);
        $role->update([
            'name' => $request->name
        ]);

        $role->syncPermissions($request->permissions);
        return redirect()->route('admin.roles')->with(['success' => 'Role updated successfully.']);
    }

    public function deleteRole($id)
    {
        // dd($id);
        Role::find($id)->delete();
        return back()->with(['success' => 'Role deleted successfully.']);

    }
}
