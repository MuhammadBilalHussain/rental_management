<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserLocation;
use App\Models\UserProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    //

    public function userProfile()
    {
        $user = User::with('UserProfile', 'UserLocation')->where('id', Auth()->id())->first();
        return view('admin.user-management.users.profile', compact('user'));
    }

    public function ProfileUpdate(Request $request)
    {

        // dd($request);

        $validatedData = $request->validate(
            [
                'business_name' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'phone_no' => 'required',

            ],
            [
                'business_name.required' => 'Business Name is Required',
                'first_name.required' => 'First Name is required.',
                'last_name.required' => 'Last Name is required.',
                'phone_no.required' => 'Phone Number is required.',

            ]
        );
        if ($request->hasFile('image') && $request->file('image') != null) {
            $user = UserProfile::where('user_id', Auth::user()->id)->first();
            if ($user->user_image) {
                $image_path = public_path('uploads/profile/' . $user->user_image);
                if (file_exists($image_path)) {
                    unlink($image_path);
                }
            }

            $image = time() . '.' . $request->file('image')->extension();
            $request['image'] = $image;
            $request->file('image')->move(public_path('uploads/profile'), $image);
        } else {
            $image = null;
        }

        User::where('id', Auth()->id())->update([
            'name' => $request->first_name . '' . $request->last_name,
        ]);
        UserProfile::updateOrCreate([
            'user_id' => Auth()->id()
        ], [
            'business_name' => $request->business_name,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone_no' => $request->phone_no,
            'gender' => $request->gender,
            'dob' => $request->birthday,
            'about_myself' => $request->about_yourself,
            'user_image' => $image,

        ]);
        return back()->with(['success' => 'Profile updated successfully.']);
    }

    public function LocationUpdate(Request $request)
    {

        // dd($request);
        // $validatedData = $request->validate(
        //     [
        //         'business_name' => 'required',
        //         'first_name' => 'required',
        //         'last_name' => 'required',
        //         'phone_no' => 'required',

        //     ],
        //     [
        //         'business_name.required' => 'Business Name is Required',
        //         'first_name.required' => 'First Name is required.',
        //         'last_name.required' => 'Last Name is required.',
        //         'phone_no.required' => 'Phone Number is required.',

        //     ]
        // );

        UserLocation::updateOrCreate([
            'user_id' => Auth()->id()
        ], [
            'address_1' => $request->address_1,
            'address_2' => $request->address_2,
            'country' => $request->country,
            'state' => $request->state,
            'city' => $request->city,
            'zip_code' => $request->zip_code,
        ]);
        return back()->with(['success' => 'Profile updated successfully.']);
    }
}
