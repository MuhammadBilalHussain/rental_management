<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function checkout(Request $request){
        $days =
        $cart = [
            "product_id" =>$request->product_id,
            "vendor_id" =>$request->vendor_id,
            "product_price" =>$request->product_price,
            "start_date" => $request->start_date,
            "end_date" => $request->end_date,
            "adults" => $request->adults,
            "childs" => $request->childs,
            "infants" => $request->infants,
            "pets" => $request->pets,
            "total_guests"=>$request->adults+$request->childs+$request->infants+$request->pets
        ];
        return view('frontend/book-room',compact('cart'));
    }
}
