<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class FrontendController extends Controller
{
    //

     public function index(){
         $searchCategory = \request('category');
         if ($searchCategory){
             $categories = Category::where('parent_id',$searchCategory)->get();
             Session::put('categories', $categories);

             $products = Product::with(['ProductFacilities', 'ProductFeatures', 'ProductOffers', 'ProductImages'])
                 ->where('category_id',$searchCategory)
                 ->get();
         }else {
             $categories = Category::whereNull('parent_id')->get();
             Session::put('categories', $categories);
             $products = Product::with(['ProductFacilities', 'ProductFeatures', 'ProductOffers', 'ProductImages'])->get();
         }
         return view('welcome', compact('products'));
     }
}
