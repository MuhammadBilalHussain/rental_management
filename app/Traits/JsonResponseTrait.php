<?php

namespace App\Traits;

trait JsonResponseTrait
{
    public function success($data,  $message=null)
    {
        return response()->json(['status'=>'success', 'data' => $data, 'message'=>$message],200);
    }
    public function error($message, $code=400)
    {
        return response()->json(['status'=>'error', 'message' => $message],$code);
    }
}
