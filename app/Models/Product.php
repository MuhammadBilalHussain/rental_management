<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $guarded=[];

    public function ProductCategory(){
        return $this->belongsTo(Category::class, 'category_id','id');
    }
    public function ProductFacilities(){
        return $this->hasMany(ProductFacility::class,'product_id','id');
    }

    public function ProductFeatures(){
        return $this->hasMany(ProductFeature::class,'product_id','id');
    }

    public function ProductOffers(){
        return $this->hasMany(ProductOffer::class,'product_id','id');
    }

    public function ProductImages(){
        return $this->hasMany(ProductImage::class,'product_id','id');
    }
}
