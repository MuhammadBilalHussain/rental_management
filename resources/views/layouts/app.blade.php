<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Real Estate</title>
    <!-- Faviicon -->
    <link rel="shortcut icon" href="{{ asset('frontend/asset/logo/faviicon.svg') }}" type="image/x-icon">
    <!-- Fontosome Style Sheet -->
    <link rel="stylesheet" href="{{ asset('frontend/asset/css/all.min.css') }}">
    <!-- Bootstarp 5 Style Sheet -->
    <link rel="stylesheet" href="{{ asset('frontend/asset/css/bootstrap.min.css') }}">
    <!-- Custom Style Sheet -->
    <link rel="stylesheet" href="{{ asset('frontend/asset/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/asset/css/index.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/asset/css/form.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/asset/css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/asset/css/swiper-bundle.min.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/asset/icons/fontawesome6/css/all.min.css') }}">

    <style>
        #swal2-title{
            font-size: 16px !important;
        }
        .zoomImage {
            transition: transform 0.8s ease;
        }

        .zoomImage:hover {
            transform: scale(1.1);
        }

    </style>
</head>
<body>

    <!-- ****** Header Section Start Here ****** -->

    @include('frontend.inc.header')

    <!-- Header Section End Here -->

    {{-- front content here --}}

    @yield('front_content')
    {{-- front content end --}}

    <!-- Footer Code Start Here -->
    @include('frontend.inc.footer')
    <!-- Footer Code End Here -->



    <!-- *******  JavaScript Files   ******* -->

    @include('frontend.inc.front_js')
    @stack('front_js')
    @include('includes.flash_messages')

</body>

</html>
