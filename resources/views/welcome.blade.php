 @extends('layouts.app')

 @section('front_content')
     <section class=" bg-light " style="padding:150px 0px;">
         <div class="container-fluid px-5 mt-3">
             <div class="row g-4">
                 @foreach($products as $index=>$product)
                 <div class="col-12 col-md-6 col-lg-3" onmouseover="animateElement(this)">
                     <div class="productcard">
                         <div class="propertydiv" >
                             <div id="items_{{$index}}" class="carousel slide" data-ride="carousel">
                                 <a href="{{ url('product-detail').'/'.$product->id }}">
                                     <div class="carousel-inner">
                                         @foreach($product->ProductImages as $ind=>$image)
                                         <div class="carousel-item {{$ind==0 ? 'active' : ''}}">
                                             <div class="propertyimage wow animate__pulse" >
                                                 <img src="{{ asset($image->image) }}" class="zoomImage"
                                                     alt="Product Image" style="height: 250px !important;width: auto !important;">
                                             </div>
                                         </div>
                                         @endforeach
                                     </div>
                                 </a>
                                 <button class="carousel-control-prev" type="button" data-bs-target="#items_{{$index}}" data-bs-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Previous</span>
                                  </button>
                                  <button class="carousel-control-next" type="button" data-bs-target="#items_{{$index}}" data-bs-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Next</span>
                                  </button>
                             </div>
                             <div class="productdetail">
                                 <a href="#">Appartment, Farm, Villa</a>
                                 <h4>Shepherdson House</h4>
                                 <p>174 Co Rd 288 Glennville</p>
                                 <div class="row">
                                     <div class="col-6"><a href="#"><i class="fas fa-vector-square"></i> 400
                                             SqFt</a></div>
                                     <div class="col-6"><a href="#"><i class="fas fa-bed"></i> 5 Bedrooms</a>
                                     </div>
                                     <div class="col-6"><a href="#"><i class="fas fa-bath"></i> 5 Bathrooms</a>
                                     </div>
                                     <div class="col-6"><a href="#"><i class="fas fa-warehouse"></i> 2
                                             Garages</a></div>
                                 </div>
                             </div>
                         </div>
                         
                     </div>
                 </div>
                 @endforeach
             </div>
         </div>
     </section>
 @endsection
