@extends('layouts.adminlayout')
@section('title-text','Add Products')

@section('content')
    <div class="card rounded">
        <div class="card-body">
            <form action="{{url('admin/save-product')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <h4>Basic Details</h4>
                <div class="row mt-2">
                    <div class="col-md-6 form-group">
                        <label for="product_name">Name</label>
                        <input type="text" class="form-control" name="product_name" id="product_name" />
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="category">Category</label>
                        <select class="form-control" name="category" id="category" >
                            <option value="">Select</option>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="price">Price</label>
                        <input type="number" class="form-control" name="price" id="price" />
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="discounted_price">Discounted Price</label>
                        <input type="number" class="form-control" name="discounted_price" id="discounted_price" />
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="main_image">Main Image</label>
                        <input type="file" class="form-control" name="main_image" id="main_image" />
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="other_images">Other Images</label>
                        <input type="file" class="form-control" name="other_images[]" id="other_images" multiple />
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="video">Video</label>
                        <input type="file" class="form-control" name="video" id="video"  />
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="short_description">Short Description</label>
                        <textarea class="form-control" name="short_description" id="short_description"></textarea>
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="long_description">Long Description</label>
                        <textarea class="form-control" name="long_description" id="long_description"></textarea>
                    </div>
                </div>
                <h4>Add Facilities</h4>
                <div class="row mt-2">
                    <div class="col-md-12 form-group">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Icon</th>
                                    <th>Detail</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="facilitiesRows">
                                <tr class="facTr">
                                    <td>
                                        <textarea class="form-control" placeholder="SVG" name="facilities[0][icon]" id="fIcon0"></textarea>
                                    </td>
                                    <td>
                                        <textarea class="form-control" placeholder="Details.." name="facilities[0][details]" id="fDet0"></textarea>
                                    </td>
                                    <td>
                                        <button class="btn btn-sm btn-primary" onclick="AddFacility()"> Add More</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>


                    </div>
                </div>
                <h4>Add Features</h4>
                <div class="row mt-2">
                    <div class="col-md-12 form-group">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Icon</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody id="featuresRows">
                            <tr class="feaTr">
                                <td>
                                    <textarea class="form-control" placeholder="SVG" name="features[0][icon]" id="feaIcon0"></textarea>
                                </td>
                                <td>
                                    <textarea class="form-control" placeholder="Title" name="features[0][details]" id="feaDet0"></textarea>
                                </td>
                                <td>
                                    <textarea class="form-control" placeholder="Description" name="features[0][description]" id="feaDesc0"></textarea>
                                </td>
                                <td>
                                    <button class="btn btn-sm btn-primary" onclick="AddFeature()"> Add More</button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <h4>Add Place Offers</h4>
                <div class="row mt-2">
                <div class="col-md-12 form-group">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Icon</th>
                            <th>Detail</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody id="offersRows">
                        <tr class="offTr">
                            <td>
                                <textarea class="form-control" placeholder="SVG" name="offer[0][icon]" id="offIcon0"></textarea>
                            </td>
                            <td>
                                <textarea class="form-control" placeholder="Details.." name="offer[0][details]" id="offDet0"></textarea>
                            </td>
                            <td>
                                <button class="btn btn-sm btn-primary" onclick="AddOffer()"> Add More</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>


                </div>
            </div>
                <button class="btn btn-success">Save</button>
            </form>
        </div>
    </div>
@endsection
@push('js')
    <script>
        function AddFacility() {
            let dataCount = $("#facilitiesRows tr.facTr").length;

            $('#facilitiesRows').append('<tr>' +
                '<td>' +
                    '<textarea class="form-control" placeholder="SVG" name="facilities['+dataCount+'][icon]" id="fIcon'+dataCount+'"></textarea>'+
                '</td>' +
                '<td>' +
                    '<textarea class="form-control" placeholder="Details.." name="facilities['+dataCount+'][details]" id="fDet'+dataCount+'"></textarea>'+
                '</td>' +
                '<td>' +
                    '<button class="btn btn-sm btn-warning" onclick="removeFacility(this)"> Remove</button>'+
                '</td>' +
            '</tr>');
        }
        function removeFacility(ele) {
            ele.closest("tr").remove();
        }
        function AddOffer() {
            let dataCount = $("#offersRows tr.offTr").length;

            $('#offersRows').append('<tr>' +
                '<td>' +
                    '<textarea class="form-control" placeholder="SVG" name="offer['+dataCount+'][icon]" id="offIcon'+dataCount+'"></textarea>'+
                '</td>' +
                '<td>' +
                    '<input class="form-control" placeholder="Details.." name="offer['+dataCount+'][details]" id="offDet'+dataCount+'" />'+
                '</td>' +
                '<td>' +
                    '<button class="btn btn-sm btn-warning" onclick="removeOffer(this)"> Remove</button>'+
                '</td>' +
            '</tr>');
        }
        function removeOffer(ele) {
            ele.closest("tr").remove();
        }

        function AddFeature() {
            let dataCount = $("#facilitiesRows tr.feaTr").length;

            $('#featuresRows').append('<tr>' +
                '<td>' +
                '<textarea class="form-control" placeholder="SVG" name="features['+dataCount+'][icon]" id="feaIcon'+dataCount+'"></textarea>'+
                '</td>' +
                '<td>' +
                '<textarea class="form-control" placeholder="Title" name="features['+dataCount+'][details]" id="feaDet'+dataCount+'"></textarea>'+
                '</td>' +
                '<td>' +
                '<textarea class="form-control" placeholder="Description" name="features['+dataCount+'][description]" id="feaDesc'+dataCount+'"></textarea>'+
                '</td>' +
                '<td>' +
                '<button class="btn btn-sm btn-warning" onclick="removeFeature(this)"> Remove</button>'+
                '</td>' +
                '</tr>');
        }
        function removeFeature(ele) {
            ele.closest("tr").remove();
        }

    </script>
@endpush
