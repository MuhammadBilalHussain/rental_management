@extends('layouts.adminlayout')
@section('title-text','Products')

@section('content')
    <div class="card rounded">
        <div class="card-header d-flex justify-content-between">
            <h4>{{ __('Products') }}</h4>
            <a href="{{url('admin/add-product')}}" class="btn btn-primary">Add Product</a>
        </div>

        <div class="card-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <td>#</td>
                    <td>Name</td>
                    <td>Category</td>
                    <td>Price</td>
                    <td>Main Image</td>
                    <td>Action</td>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $product)
                    <tr>
                        <td>{{$product->id}}</td>
                        <td>{{$product->name}}</td>
                        <td>{{$product->ProductCategory->name}}</td>
                        <td>{{$product->price}}</td>
                        <td><img src="{{asset($product->product_image)}}" alt="" width="100" height="50"></td>
                        <td>
                            <a href="#" class="btn btn-secondary btn-sm">Edit</a>
                            <a href="#" class="btn btn-danger btn-sm">Delete</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
