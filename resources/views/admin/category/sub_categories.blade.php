@extends('layouts.adminlayout')
@section('title-text', 'Sub Categories')

@section('content')
    <div class="card rounded">
        <div class="card-header d-flex justify-content-between">
            <h4>{{ __('Sub Categories') }}</h4>
            @can('add-sub-category')
                <a href="#" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">Add Sub
                    Category</a>
            @endcan
        </div>

        <div class="card-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>Name</td>
                        <td>Parent Name</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($categories as $category)
                        <tr>
                            <td>{{ $category->id }}</td>
                            <td>{{ $category->name }}</td>
                            <td>{{ $category->Parent->name }}</td>
                            <td>
                                @can('edit-sub-category')
                                    <button class="btn  btn-primary edit-user" data-bs-toggle="modal"
                                        data-bs-target="#editcategory" data-id="{{ $category->id }}"
                                        data-name="{{ $category->name }}" data-parent="{{ $category->Parent->id }}"
                                        data-icon="{{ $category->icon }}">Edit</button>
                                @endcan
                                @can('delete-sub-category')
                                    <button class="btn  btn-danger delete-cate" data-bs-toggle="modal"
                                        data-bs-target="#deletecategory" data-id="{{ $category->id }}">Delete</button>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
            aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <form action="{{ route('admin.storesubCategory') }}" method="post">
                    @csrf
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="staticBackdropLabel">Add Sub Category</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12 mb-3">
                                    <label for="name" class="form-label">Name</label>
                                    <input type="text" class="form-control" name="name">
                                </div>
                                {{-- <div class="col-md-6 mb-3">
                                    <label for="slug">Slug</label>
                                    <input type="text" class="form-control" name="slug">
                                </div> --}}

                                <div class="col-md-12 mb-3">
                                    <label for="name" class="form-label">Parent</label>
                                    <select name="parent" id="parent" class="form-control">
                                        <option value="">--select category--</option>
                                        @foreach ($all_categories as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-12">
                                    <label for="slug" class="form-label">Icon</label>
                                    <textarea name="icon" rows="3" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
        {{-- edit --}}
        <div class="modal fade" id="editcategory" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
            aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <form method="post" id="editUserForm">
                    @csrf
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="staticBackdropLabel">Edit Sub Category</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12 mb-3">
                                    <label for="name" class="form-label">Name</label>
                                    <input type="text" class="form-control" name="name" id="name">
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label for="name" class="form-label">Parent</label>
                                    <select name="parent" id="parent" class="form-control">
                                        <option value="">--select category--</option>
                                        @foreach ($all_categories as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-12">
                                    <label for="slug" class="form-label">Icon</label>
                                    <textarea name="icon" id="icon" rows="3" class="form-control" id="icon"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
        <div class="modal fade" id="deletecategory" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
            aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <form method="post" id="deletecate">
                    @csrf
                    @method('DELETE')
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="staticBackdropLabel">Delete Sub Category</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <p>Are you shure to delete this!</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Delete</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
    @push('js')
        <script>
            $(document).ready(function() {
                // Edit User Modal
                $('.edit-user').click(function() {
                    var category_id = $(this).data('id');
                    var name = $(this).data('name');
                    var icon = $(this).data('icon');
                    var parent = $(this).data('parent');


                    $('#editUserForm').attr('action', 'edit-sub-category/' + category_id);
                    $('#name').val(name);
                    $('#icon').val(icon);
                    $("#parent option[value='" + parent + "']").attr("selected", "selected");

                });
            });
        </script>
        <script>
            $(document).ready(function() {
                // Edit User Modal
                $('.delete-cate').click(function() {
                    var id = $(this).data('id');
                    $('#deletecate').attr('action', 'delete-sub-category/' + id);

                });
            });
        </script>
    @endpush
@endsection
