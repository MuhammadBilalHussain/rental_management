@extends('layouts.adminlayout')
@section('title-text', 'Users')

@section('content')
    <div class="card rounded">
        <div class="card-header d-flex justify-content-between">
            <h4>{{ __('Users') }}</h4>
           
        </div>

        <div class="card-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>First Name</td>
                        <td>Last Name</td>
                        <td>Email</td>
                        <td>Phone Number</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $per)
                        <tr>
                            <td>{{ $per->id }}</td>
                            <td>{{ @$per->UserProfile->first_name ? @$per->UserProfile->first_name : 'N/A'}}</td>
                            <td>{{ @$per->UserProfile->last_name ? @$per->UserProfile->last_name : 'N/A'}}</td>
                            <td>{{ $per->email }}</td>
                            <td>{{ @$per->UserProfile->phone_no ? @$per->UserProfile->phone_no : 'N/A' }}</td>
                            <td>
                                <button class="btn  btn-primary edit-user btn-sm" data-bs-toggle="modal" data-bs-target="#edituser" data-id="{{ $per->id }}"
                                    data-first_name="{{ @$per->UserProfile->first_name }}"  data-last_name="{{ @$per->UserProfile->last_name }}" data-phone_no="{{ @$per->UserProfile->phone_no}}">Edit</button>
                                    <button class="btn  btn-danger delete-permission btn-sm" data-bs-toggle="modal" data-bs-target="#deleteuser" data-id="{{ $per->id }}"
                                       >Delete</button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

       

        {{-- edit --}}
        <div class="modal fade" id="edituser" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
            aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <form  method="post" id="editUserForm">
                    @csrf
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="staticBackdropLabel">Edit User</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12 mb-3">
                                    <label for="name" class="form-label">First Name</label>
                                    <input type="text" class="form-control" name="first_name" id="first_name">
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label for="slug" class="form-label">Last Name</label>
                                    <input type="text" class="form-control" name="last_name" id="last_name">
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label for="slug" class="form-label">Phone  No</label>
                                    <input type="text" class="form-control" name="phone_no" id="phone_no">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
        <div class="modal fade" id="deleteuser" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
            aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <form  method="post" id="deleteper">
                    @csrf
                    @method('DELETE')
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="staticBackdropLabel">Delete User</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <p>Are you shure to delete this!</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Delete</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>

    </div>

    @push('js')
        <script>
            $(document).ready(function() {
                // Edit User Modal
                $('.edit-user').click(function() {
                    var user_id = $(this).data('id');
                    var first_name = $(this).data('first_name');
                    var last_name = $(this).data('last_name');
                    var phone_no = $(this).data('phone_no');


                    $('#editUserForm').attr('action', 'edit-user-profile/' + user_id);
                    $('#first_name').val(first_name);
                    $('#last_name').val(last_name);
                    $('#phone_no').val(phone_no);



                });
            });
        </script>
        <script>
            $(document).ready(function() {
                // Edit User Modal
                $('.delete-permission').click(function() {
                    var id = $(this).data('id');
                    $('#deleteper').attr('action', 'delete-user/' + id);

                });
            });
        </script>
    @endpush

@endsection
