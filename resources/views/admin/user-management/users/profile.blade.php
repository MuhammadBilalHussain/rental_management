@extends('layouts.adminlayout')
@section('title-text', 'Profile')

@section('content')
    <div class="conatiner-fluid content-inner mt-n5 py-0">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-wrap align-items-center justify-content-between">
                            <div class="d-flex flex-wrap align-items-center">
                                <div class="profile-img position-relative me-3 mb-3 mb-lg-0 profile-logo profile-logo1">
                                    @if (@$user->UserProfile->user_image)
                                        <img src="{{ asset('uploads/profile/' . @$user->UserProfile->user_image) }}"
                                            alt="User-Profile"
                                            class="theme-color-default-img img-fluid rounded-pill avatar-100">
                                    @else
                                        <img src="../../assets/images/avatars/01.png" alt="User-Profile"
                                            class="theme-color-default-img img-fluid rounded-pill avatar-100">
                                    @endif
                                </div>
                                <div class="d-flex flex-wrap align-items-center mb-3 mb-sm-0">
                                    <h4 class="me-2 h4">{{ $user->name }}</h4>
                                </div>
                            </div>
                            <ul class="d-flex nav nav-pills mb-0 text-center profile-tab nav-slider"
                                data-toggle="slider-tab" id="profile-pills-tab" role="tablist">
                                <li class="nav-item ">
                                    <a class="nav-link active" data-bs-toggle="tab" href="#profile-feed" role="tab"
                                        aria-selected="false">Personal Information</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="tab" href="#profile-activity" role="tab"
                                        aria-selected="false">Location Information</a>
                                </li>


                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="profile-content tab-content">
                    <div id="profile-feed" class="tab-pane fade">
                        <div class="card">
                            <div class="card-body">
                                <div class="new-user-info">
                                    <form action="{{ route('admin.UpdateProfile') }}" method="post"
                                        enctype="multipart/form-data">
                                        @csrf
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="form-label" for="exampleInputText1">Business Name </label>
                                                <div class="input-group mb-3">
                                                    <span class="input-group-text" id="basic-addon1">
                                                        <svg width="20" viewBox="0 0 24 24" fill="none"
                                                            xmlns="http://www.w3.org/2000/svg">
                                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                                d="M9.59151 15.2068C13.2805 15.2068 16.4335 15.7658 16.4335 17.9988C16.4335 20.2318 13.3015 20.8068 9.59151 20.8068C5.90151 20.8068 2.74951 20.2528 2.74951 18.0188C2.74951 15.7848 5.88051 15.2068 9.59151 15.2068Z"
                                                                stroke="currentColor" stroke-width="1.5"
                                                                stroke-linecap="round" stroke-linejoin="round"></path>
                                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                                d="M9.59157 12.0198C7.16957 12.0198 5.20557 10.0568 5.20557 7.63476C5.20557 5.21276 7.16957 3.24976 9.59157 3.24976C12.0126 3.24976 13.9766 5.21276 13.9766 7.63476C13.9856 10.0478 12.0356 12.0108 9.62257 12.0198H9.59157Z"
                                                                stroke="currentColor" stroke-width="1.5"
                                                                stroke-linecap="round" stroke-linejoin="round"></path>
                                                            <path
                                                                d="M16.4829 10.8815C18.0839 10.6565 19.3169 9.28253 19.3199 7.61953C19.3199 5.98053 18.1249 4.62053 16.5579 4.36353"
                                                                stroke="currentColor" stroke-width="1.5"
                                                                stroke-linecap="round" stroke-linejoin="round"></path>
                                                            <path
                                                                d="M18.5952 14.7322C20.1462 14.9632 21.2292 15.5072 21.2292 16.6272C21.2292 17.3982 20.7192 17.8982 19.8952 18.2112"
                                                                stroke="currentColor" stroke-width="1.5"
                                                                stroke-linecap="round" stroke-linejoin="round"></path>
                                                        </svg> </span>
                                                    <input type="text" class="form-control" name="business_name"
                                                        id="business_name" value="{{ @$user->UserProfile->business_name }}">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="form-label" for="exampleInputText1">Username </label>
                                                <div class="input-group mb-3">
                                                    <span class="input-group-text" id="basic-addon1"> <svg width="20"
                                                            viewBox="0 0 24 24" fill="none"
                                                            xmlns="http://www.w3.org/2000/svg">
                                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                                d="M11.9849 15.3462C8.11731 15.3462 4.81445 15.931 4.81445 18.2729C4.81445 20.6148 8.09636 21.2205 11.9849 21.2205C15.8525 21.2205 19.1545 20.6348 19.1545 18.2938C19.1545 15.9529 15.8735 15.3462 11.9849 15.3462Z"
                                                                stroke="currentColor" stroke-width="1.5"
                                                                stroke-linecap="round" stroke-linejoin="round"></path>
                                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                                d="M11.9849 12.0059C14.523 12.0059 16.5801 9.94779 16.5801 7.40969C16.5801 4.8716 14.523 2.81445 11.9849 2.81445C9.44679 2.81445 7.3887 4.8716 7.3887 7.40969C7.38013 9.93922 9.42394 11.9973 11.9525 12.0059H11.9849Z"
                                                                stroke="currentColor" stroke-width="1.42857"
                                                                stroke-linecap="round" stroke-linejoin="round"></path>
                                                        </svg> </span>
                                                    <input type="text" class="form-control" name="Username"
                                                        id="Username" value="{{ $user->name }}" readonly>
                                                </div>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label class="form-label" for="exampleInputText1">First Name </label>
                                                <div class="input-group mb-3">
                                                    <span class="input-group-text" id="basic-addon1"> <svg width="20"
                                                            viewBox="0 0 24 24" fill="none"
                                                            xmlns="http://www.w3.org/2000/svg">
                                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                                d="M11.9849 15.3462C8.11731 15.3462 4.81445 15.931 4.81445 18.2729C4.81445 20.6148 8.09636 21.2205 11.9849 21.2205C15.8525 21.2205 19.1545 20.6348 19.1545 18.2938C19.1545 15.9529 15.8735 15.3462 11.9849 15.3462Z"
                                                                stroke="currentColor" stroke-width="1.5"
                                                                stroke-linecap="round" stroke-linejoin="round"></path>
                                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                                d="M11.9849 12.0059C14.523 12.0059 16.5801 9.94779 16.5801 7.40969C16.5801 4.8716 14.523 2.81445 11.9849 2.81445C9.44679 2.81445 7.3887 4.8716 7.3887 7.40969C7.38013 9.93922 9.42394 11.9973 11.9525 12.0059H11.9849Z"
                                                                stroke="currentColor" stroke-width="1.42857"
                                                                stroke-linecap="round" stroke-linejoin="round"></path>
                                                        </svg> </span>
                                                    <input type="text" class="form-control" name="first_name"
                                                        id="first_name" value="{{ @$user->UserProfile->first_name }}">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="form-label" for="exampleInputText1">Last Name </label>
                                                <div class="input-group mb-3">
                                                    <span class="input-group-text" id="basic-addon1"> <svg width="20"
                                                            viewBox="0 0 24 24" fill="none"
                                                            xmlns="http://www.w3.org/2000/svg">
                                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                                d="M11.9849 15.3462C8.11731 15.3462 4.81445 15.931 4.81445 18.2729C4.81445 20.6148 8.09636 21.2205 11.9849 21.2205C15.8525 21.2205 19.1545 20.6348 19.1545 18.2938C19.1545 15.9529 15.8735 15.3462 11.9849 15.3462Z"
                                                                stroke="currentColor" stroke-width="1.5"
                                                                stroke-linecap="round" stroke-linejoin="round"></path>
                                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                                d="M11.9849 12.0059C14.523 12.0059 16.5801 9.94779 16.5801 7.40969C16.5801 4.8716 14.523 2.81445 11.9849 2.81445C9.44679 2.81445 7.3887 4.8716 7.3887 7.40969C7.38013 9.93922 9.42394 11.9973 11.9525 12.0059H11.9849Z"
                                                                stroke="currentColor" stroke-width="1.42857"
                                                                stroke-linecap="round" stroke-linejoin="round"></path>
                                                        </svg> </span>
                                                    <input type="text" class="form-control" name="last_name"
                                                        id="last_name" value="{{ @$user->UserProfile->last_name }}">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="form-label" for="exampleInputText1">Email</label>
                                                <div class="input-group mb-3">
                                                    <span class="input-group-text" id="basic-addon1">@</span>
                                                    <input type="text" class="form-control" name="email"
                                                        id="email" value="{{ $user->email }}" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="form-label" for="exampleInputText1">Phone Number </label>
                                                <div class="input-group mb-3">
                                                    <span class="input-group-text" id="basic-addon1"> <svg width="20"
                                                            viewBox="0 0 24 24" fill="none"
                                                            xmlns="http://www.w3.org/2000/svg">
                                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                                d="M11.5317 12.4724C15.5208 16.4604 16.4258 11.8467 18.9656 14.3848C21.4143 16.8328 22.8216 17.3232 19.7192 20.4247C19.3306 20.737 16.8616 24.4943 8.1846 15.8197C-0.493478 7.144 3.26158 4.67244 3.57397 4.28395C6.68387 1.17385 7.16586 2.58938 9.61449 5.03733C12.1544 7.5765 7.54266 8.48441 11.5317 12.4724Z"
                                                                stroke="currentColor" stroke-width="1.5"
                                                                stroke-linecap="round" stroke-linejoin="round"></path>
                                                        </svg> </span>
                                                    <input type="text" class="form-control" name="phone_no"
                                                        id="phone_no" value="{{ @$user->UserProfile->phone_no }}">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="form-label" for="exampleInputText1">DOB</label>
                                                <div class="input-group mb-3">
                                                    <span class="input-group-text" id="basic-addon1"> <svg width="20"
                                                            viewBox="0 0 24 24" fill="none"
                                                            xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M3.09277 9.40421H20.9167" stroke="currentColor"
                                                                stroke-width="1.5" stroke-linecap="round"
                                                                stroke-linejoin="round"></path>
                                                            <path d="M16.442 13.3097H16.4512" stroke="currentColor"
                                                                stroke-width="1.5" stroke-linecap="round"
                                                                stroke-linejoin="round"></path>
                                                            <path d="M12.0045 13.3097H12.0137" stroke="currentColor"
                                                                stroke-width="1.5" stroke-linecap="round"
                                                                stroke-linejoin="round"></path>
                                                            <path d="M7.55818 13.3097H7.56744" stroke="currentColor"
                                                                stroke-width="1.5" stroke-linecap="round"
                                                                stroke-linejoin="round"></path>
                                                            <path d="M16.442 17.1962H16.4512" stroke="currentColor"
                                                                stroke-width="1.5" stroke-linecap="round"
                                                                stroke-linejoin="round"></path>
                                                            <path d="M12.0045 17.1962H12.0137" stroke="currentColor"
                                                                stroke-width="1.5" stroke-linecap="round"
                                                                stroke-linejoin="round"></path>
                                                            <path d="M7.55818 17.1962H7.56744" stroke="currentColor"
                                                                stroke-width="1.5" stroke-linecap="round"
                                                                stroke-linejoin="round"></path>
                                                            <path d="M16.0433 2V5.29078" stroke="currentColor"
                                                                stroke-width="1.5" stroke-linecap="round"
                                                                stroke-linejoin="round"></path>
                                                            <path d="M7.96515 2V5.29078" stroke="currentColor"
                                                                stroke-width="1.5" stroke-linecap="round"
                                                                stroke-linejoin="round"></path>
                                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                                d="M16.2383 3.5791H7.77096C4.83427 3.5791 3 5.21504 3 8.22213V17.2718C3 20.3261 4.83427 21.9999 7.77096 21.9999H16.229C19.175 21.9999 21 20.3545 21 17.3474V8.22213C21.0092 5.21504 19.1842 3.5791 16.2383 3.5791Z"
                                                                stroke="currentColor" stroke-width="1.5"
                                                                stroke-linecap="round" stroke-linejoin="round"></path>
                                                        </svg> </span>
                                                    <input type="date" class="form-control" name="birthday"
                                                        id="birthday" value="{{ @$user->UserProfile->dob }}">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="form-label" for="exampleInputText1">Gender</label>
                                                <div class="input-group mb-3">
                                                    <span class="input-group-text" id="basic-addon1">@</span>
                                                    <select class="form-select  shadow-none form-control" name="gender"
                                                        id="gender">
                                                        <option value="Male"
                                                            {{ @$user->UserProfile->gender == 'Male' ? 'selected' : '' }}>
                                                            Male</option>
                                                        <option value="Female"
                                                            {{ @$user->UserProfile->gender == 'Female' ? 'selected' : '' }}>
                                                            Female</option>
                                                        <option value="Other"
                                                            {{ @$user->UserProfile->gender == 'Other' ? 'selected' : '' }}>
                                                            Other</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label class="form-label" for="exampleFormControlTextarea1">About
                                                    Yourself</label>
                                                <textarea class="form-control" id="about_yourself" name="about_yourself" rows="2">{{ @$user->UserProfile->about_myself }}</textarea>
                                            </div>
                                            <div class="col-md-12">
                                                <label class="form-label">Profile Pic</label>
                                                <input type="file" class="form-control" name="image"
                                                    id="selectImage">
                                            </div>

                                            <div class="col-md-3">
                                                <img id="preview" src="#" alt="your image"
                                                    class="mt-3 img-fluid" style="display:none;" />
                                            </div>
                                            <div>
                                                <button class="btn btn-primary float-end">Update</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div id="profile-activity" class="tab-pane fade">
                        <div class="card">
                            <div class="card-body">
                                <div class="new-user-info">
                                    <form action="{{ route('admin.UpdateUserLocation') }}" method="post">
                                        @csrf
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="form-label" for="exampleFormControlTextarea1">Address Line
                                                    1</label>
                                                <textarea class="form-control" id="address_1" name="address_1" rows="2">{{ @$user->UserLocation->address_1 }}</textarea>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="form-label" for="exampleFormControlTextarea1">Address Line
                                                    2</label>
                                                <textarea class="form-control" id="address_2" name="address_2" rows="2">{{ @$user->UserLocation->address_2 }}</textarea>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label class="form-label" for="exampleInputText1">City</label>
                                                <div class="input-group mb-3">
                                                    <span class="input-group-text" id="basic-addon1">@</span>
                                                    <input type="text" class="form-control" name="city"
                                                        id="city" value="{{ @$user->UserLocation->city }}">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="form-label" for="exampleInputText1">State</label>
                                                <div class="input-group mb-3">
                                                    <span class="input-group-text" id="basic-addon1">@</span>
                                                    <input type="text" class="form-control" name="state"
                                                        id="state" value="{{ @$user->UserLocation->state }}">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="form-label" for="exampleInputText1">Country</label>
                                                <div class="input-group mb-3">
                                                    <span class="input-group-text" id="basic-addon1">@</span>
                                                    <input type="text" class="form-control" name="country"
                                                        id="country" value="{{ @$user->UserLocation->country }}">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="form-label" for="exampleInputText1">Zip Code </label>
                                                <div class="input-group mb-3">
                                                    <span class="input-group-text" id="basic-addon1">@</span>
                                                    <input type="text" class="form-control" name="zip_code"
                                                        id="zip_code" value="{{ @$user->UserLocation->zip_code }}">
                                                </div>
                                            </div>

                                            <div>
                                                <button class="btn btn-primary float-end">Update</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>
    @push('js')
        <script>
            selectImage.onchange = evt => {
                preview = document.getElementById('preview');
                preview.style.display = 'block';
                const [file] = selectImage.files
                if (file) {
                    preview.src = URL.createObjectURL(file)
                }
            }
        </script>
    @endpush
@endsection
