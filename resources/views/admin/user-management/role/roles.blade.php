a@extends('layouts.adminlayout')
@section('title-text', 'Roles')

@section('content')
    <div class="card rounded">
        <div class="card-header d-flex justify-content-between">
            <h4>{{ __('Roles') }}</h4>
            @can('add-role')
                <a href="{{ url('admin/add-role') }}" class="btn btn-primary">Add Role</a>
            @endcan
        </div>

        <div class="card-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>Name</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($roles as $role)
                        <tr>
                            <td>{{ $role->id }}</td>
                            <td>{{ $role->name }}</td>
                            <td>
                                @can('edit-role')
                                    <a href="{{ url('admin/edit-role/' . $role->id) }}"
                                        class="btn  btn-primary edit-user">Edit</a>
                                @endcan
                                @can('delete-role')
                                    <button class="btn  btn-danger delete-role" data-bs-toggle="modal"
                                        data-bs-target="#deleterole" data-id="{{ $role->id }}">Delete</button>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
    {{-- delete --}}
    <div class="modal fade" id="deleterole" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <form method="post" id="delete_role">
                @csrf
                @method('DELETE')
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Delete Role</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p>Are you shure to delete this!</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Delete</button>
                    </div>
                </div>
            </form>

        </div>
    </div>

    @push('js')
        <script>
            $(document).ready(function() {
                // Edit User Modal
                $('.delete-role').click(function() {
                    var id = $(this).data('id');
                    $('#delete_role').attr('action', 'delete-role/' + id);

                });
            });
        </script>
    @endpush
@endsection
