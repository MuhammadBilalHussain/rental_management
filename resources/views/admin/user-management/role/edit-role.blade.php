@extends('layouts.adminlayout')
@section('title-text', 'Roles')

@section('content')
    <div class="row">
        <div class="col-lg-8 mx-auto">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">Edit Role</h4>
                    </div>
                </div>
                <div class="card-body">
                    <div class="new-user-info">
                        <form method="POST" action="{{ route('admin.UpdateRole',[$role->id]) }}">
                            @csrf
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="form-label" for="fname">Role Name:</label>
                                    <input type="text" class="form-control" id="name" name="name"
                                        value="{{ $role->name }}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="form-label" for="lname">Guard Name:</label>
                                    <input type="text" class="form-control" id="guard_name"
                                        value="{{ $role->guard_name }}" readonly>
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="form-label" for="add1">Select Permissions:</label>
                                    <select class="js-example-basic-multiple form-control" name="permissions[]"
                                        multiple="multiple">
                                        @foreach ($permissions as $permission)
                                            <option {{ in_array($permission->id, $rolePermissions) ? 'selected' : '' }}>
                                                {{ $permission->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="my-3">
                                <button type="submit" class="btn btn-primary float-end">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('js')
        <script>
            $(document).ready(function() {
                $('.js-example-basic-multiple').select2();
            });
        </script>
    @endpush

@endsection
