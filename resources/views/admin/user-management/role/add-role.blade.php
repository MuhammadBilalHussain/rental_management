@extends('layouts.adminlayout')
@section('title-text', 'Roles')

@section('content')
    <div class="row">
        <div class="col-lg-8 mx-auto">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">Add Role</h4>
                    </div>
                </div>
                <div class="card-body">
                    <div class="new-user-info">
                        <form method="POST" action="{{ route('admin.storeRole') }}">
                            @csrf
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="form-label" for="fname">Role Name:</label>
                                    <input type="text" class="form-control" id="name" name="name"
                                        value="{{ old('name') }}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="form-label" for="lname">Guard Name:</label>
                                    <input type="text" class="form-control" id="guard_name" name="guard_name"
                                        value="{{ old('guard_name') }}">
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="form-label" for="add1">Select Permissions:</label>
                                    <select class="js-example-basic-multiple form-control" name="permissions[]"
                                        multiple="multiple">
                                        @foreach ($permissions as $per)
                                            <option value="{{ $per->id }}">{{ $per->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="">
                                <button type="submit" class="btn btn-primary float-end">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('js')
        <script>
            $(document).ready(function() {
                $('.js-example-basic-multiple').select2();
            });
        </script>
    @endpush

@endsection
