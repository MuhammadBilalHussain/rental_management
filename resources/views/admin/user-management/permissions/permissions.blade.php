@extends('layouts.adminlayout')
@section('title-text', 'Permissions')

@section('content')
    <div class="card rounded">
        <div class="card-header d-flex justify-content-between">
            <h4>{{ __('Permissions') }}</h4>
            @can('add-permission')
                <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">Add Permission</button>
            @endcan
        </div>

        <div class="card-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>Name</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($permissions as $per)
                        <tr>
                            <td>{{ $per->id }}</td>
                            <td>{{ $per->name }}</td>
                            <td>
                                @can('edit-permission')
                                    <button class="btn  btn-primary edit-user" data-bs-toggle="modal"
                                        data-bs-target="#editpermission" data-id="{{ $per->id }}"
                                        data-name="{{ $per->name }}" data-guard="{{ $per->guard_name }}">Edit</button>
                                @endcan
                                @can('delete-permission')
                                    <button class="btn  btn-danger delete-permission" data-bs-toggle="modal"
                                        data-bs-target="#deletepermission" data-id="{{ $per->id }}">Delete</button>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
            aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <form action="{{ route('admin.storePermissions') }}" method="post">
                    @csrf
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="staticBackdropLabel">Add Permissions</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12 mb-3">
                                    <label for="name" class="form-label">Permissions Name</label>
                                    <input type="text" class="form-control" name="name">
                                </div>
                                <div class="col-md-12">
                                    <label for="slug" class="form-label">Guard Name</label>
                                    <input type="text" class="form-control" name="guard">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>

        {{-- edit --}}
        <div class="modal fade" id="editpermission" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
            aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <form method="post" id="editUserForm">
                    @csrf
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="staticBackdropLabel">Edit Permission</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12 mb-3">
                                    <label for="name" class="form-label">Permissions Name</label>
                                    <input type="text" class="form-control" name="name" id="name">
                                </div>
                                <div class="col-md-12">
                                    <label for="slug" class="form-label">Guard Name</label>
                                    <input type="text" class="form-control" name="guard" id="guard" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
        <div class="modal fade" id="deletepermission" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
            aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <form method="post" id="deleteper">
                    @csrf
                    @method('DELETE')
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="staticBackdropLabel">Delete Permission</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <p>Are you shure to delete this!</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Delete</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>

    </div>

    @push('js')
        <script>
            $(document).ready(function() {
                // Edit User Modal
                $('.edit-user').click(function() {
                    var permission_id = $(this).data('id');
                    var name = $(this).data('name');
                    var guard = $(this).data('guard');

                    $('#editUserForm').attr('action', 'edit-permission/' + permission_id);
                    $('#name').val(name);
                    $('#guard').val(guard);


                });
            });
        </script>
        <script>
            $(document).ready(function() {
                // Edit User Modal
                $('.delete-permission').click(function() {
                    var id = $(this).data('id');
                    $('#deleteper').attr('action', 'delete-permissions/' + id);

                });
            });
        </script>
    @endpush

@endsection
