@extends('layouts.app')

@section('front_content')
    <section class="" style="margin-top:150px">
        <div class="container py-5 ">

            <h3 class="d-flex align-items-center"> <span class="me-3"><a href="" class=""><svg viewBox="0 0 32 32"
                            xmlns="http://www.w3.org/2000/svg" aria-label="Back" role="img" focusable="false"
                            style="display: block; fill: none; height: 16px; width: 16px; stroke: currentcolor; stroke-width: 3; overflow: visible;">
                            <g fill="none">
                                <path
                                    d="m20 28-11.29289322-11.2928932c-.39052429-.3905243-.39052429-1.0236893 0-1.4142136l11.29289322-11.2928932">
                                </path>
                            </g>
                        </svg></a></span> Request to book</h3>
            <div class="row g-4">
                <div class="col-lg-6 px-5">
                    <div class="my-3">
                        <h5>Your trip</h5>
                        <div class="d-flex justify-content-between align-items-center  my-3">
                            <div>
                                <h6><strong>Dates</strong></h6>
                                <p>FROM ({{$cart['start_date']}}) TO ({{$cart['end_date']}})</p>
                            </div>
                            <div>
                                <h6><a href="" class="text-dark text-decoration-underline" data-bs-toggle="modal"
                                        data-bs-target="#dateEdit">Edit</a></h6>
                            </div>
                        </div>
                        <div class="d-flex justify-content-between align-items-center  my-3">
                            <div>
                                <h6><strong>Guests</strong></h6>
                                <p>{{$cart['total_guests']}} guest</p>
                            </div>
                            <div>
                                <h6><a href="" class="text-dark text-decoration-underline" data-bs-toggle="modal"
                                        data-bs-target="#guestEdit">Edit</a></h6>
                            </div>
                        </div>
                    </div>
                    <div class="my-4">
                        <hr>
                    </div>
                    <h5 class="mb-3">Choose how to pay</h5>
                    <div class="row px-2">
                        <div class="col-12 pay-choose-1 active">
                            <div class="d-flex justify-content-between align-items-center">
                                <div>
                                    <h6>Pay in full</h6>
                                    <p>Pay the total ($460.98) now and you're all set.</p>
                                </div>
                                <div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="exampleRadios"
                                            id="exampleRadios2" value="option2">

                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-12 pay-choose-2">
                            <div class="d-flex justify-content-between align-items-center">
                                <div>
                                    <h6>Pay part now, part later</h6>
                                    <p>$230.49 due today, $230.49 on Jul 23, 2023. No extra fees.</p>
                                </div>
                                <div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="exampleRadios"
                                            id="exampleRadios2" value="option2">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="my-4">
                        <hr>
                    </div>
                    <h5 class="mb-3">Log in or sign up to book</h5>
                    <div class="row px-2">
                        <div class="col-12 pay-number-1 active">
                            <div class="form-floating ">
                                <select class="form-select pay-number-1-select" id="floatingSelect"
                                    aria-label="Floating label select example">
                                    <option selected>--select--</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>
                                <label for="floatingSelect">Country/Region</label>
                            </div>

                        </div>
                        <div class="col-12 pay-number-2">
                            <div class="form-floating">
                                <input type="email" class="form-control border-0" id="floatingInput"
                                    placeholder="name@example.com">
                                <label for="floatingInput">Phone Number</label>
                            </div>
                        </div>
                    </div>
                    <div class="row my-4">
                        <div class="col-12">
                            <button class="continue-btn">Continue</button>
                        </div>
                    </div>

                    <div class="row g-4 my-3">
                        <div class="col-lg-4 align-content-stretch">
                            <div class="login-share-btn">
                                <a href=""><svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" aria-hidden="true"
                                    role="presentation" focusable="false"
                                    style="display: block; height: 20px; width: 20px;">
                                    <path fill="#1877F2" d="M32 0v32H0V0z"></path>
                                    <path
                                        d="M22.938 16H18.5v-3.001c0-1.266.62-2.499 2.607-2.499h2.018V6.562s-1.831-.312-3.582-.312c-3.654 0-6.043 2.215-6.043 6.225V16H9.436v4.625H13.5V32h5V20.625h3.727l.71-4.625z"
                                        fill="#FFF"></path>
                                </svg></a>
                            </div>

                        </div>
                        <div class="col-lg-4 align-content-stretch">
                            <div class="login-share-btn ">
                                <a href="">
                                    <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" aria-hidden="true"
                                    role="presentation" focusable="false"
                                    style="display: block; height: 20px; width: 20px;">
                                    <g transform="translate(4.376957 4.073369)">
                                        <path
                                            d="m19.7480429 20.9266305c2.813-2.625 4.063-7 3.313-11.18799996h-11.188v4.62599996h6.375c-.25 1.5-1.125 2.75-2.375 3.562z"
                                            fill="#4285f4"></path>
                                        <path
                                            d="m1.24804285 17.2396305c.82223 1.6196 2.0014 3.0314 3.4486 4.129s3.1247 1.8523 4.906 2.2073c1.78130005.355 3.62000005.301 5.37740005-.1579s3.3877-1.3108 4.768-2.4914l-3.875-3c-3.313 2.188-8.81300005 1.375-10.68800005-3.75z"
                                            fill="#34a853"></path>
                                        <path
                                            d="m5.18573285 14.1766305c-.5-1.563-.5-3 0-4.56299996l-3.938-3.062c-1.438 2.875-1.875 6.93799996 0 10.68799996z"
                                            fill="#fbbc02"></path>
                                        <path
                                            d="m5.18604285 9.61463054c1.374-4.31301 7.25000005-6.81301 11.18700005-3.126l3.438-3.37401c-4.875-4.688-14.37500005-4.5-18.56300005 3.43601l3.938 3.063z"
                                            fill="#ea4335"></path>
                                    </g>
                                </svg>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 align-content-stretch">
                            <div class="login-share-btn">
                                <a href="">
                                    <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" role="presentation"
                                    aria-hidden="true" focusable="false"
                                    style="height: 20px; width: 20px; display: block; fill: currentcolor;">
                                    <path
                                        d="m13.3 2.1a5.1 5.1 0 0 1 3.8-2.1 5.1 5.1 0 0 1 -1.2 3.8 4.1 4.1 0 0 1 -3.6 1.7 4.5 4.5 0 0 1 1-3.4zm-5 3.7c-2.8 0-5.8 2.5-5.8 7.3 0 4.9 3.5 10.9 6.3 10.9 1 0 2.5-1 4-1s2.6.9 4 .9c3.1 0 5.3-6.4 5.3-6.4a5.3 5.3 0 0 1 -3.2-4.9 5.2 5.2 0 0 1 2.6-4.5 5.4 5.4 0 0 0 -4.7-2.4c-2 0-3.5 1.1-4.3 1.1-.9 0-2.4-1-4.2-1z">
                                    </path>
                                </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row my-3 px-3">
                        <a href="" class="email-share-btn"> <span><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" aria-hidden="true" role="presentation" focusable="false" style="display: block; height: 20px; width: 20px; fill: currentcolor;"><path d="M30.5105,5.87921A5.06207,5.06207,0,0,0,26,3H6A5.06215,5.06215,0,0,0,1.48932,5.87946,4.9444,4.9444,0,0,0,1,8V24a5.00588,5.00588,0,0,0,5,5H26a5.00588,5.00588,0,0,0,5-5V8A4.94373,4.94373,0,0,0,30.5105,5.87921ZM6,5H26a2.97234,2.97234,0,0,1,1.77252.59253l-9.8194,8.41675a2.98373,2.98373,0,0,1-3.90527,0l-9.82013-8.417A2.97238,2.97238,0,0,1,6,5ZM29,24a3.00328,3.00328,0,0,1-3,3H6a3.00328,3.00328,0,0,1-3-3V8a2.96566,2.96566,0,0,1,.10266-.73773l9.64343,8.26556a4.97272,4.97272,0,0,0,6.50782,0L28.89728,7.262A2.96542,2.96542,0,0,1,29,8Z"></path></svg></span> Continue with email</a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card shadow-sm  px-3 m-auto" style="width:70%;border-radius:8px;">
                        <div class="card-body">
                            <div class="row h-50">
                                <div class="col-4">
                                    <img src="{{ asset('frontend/asset/images/city/c3.jpg') }}  " alt="City Image"
                                    class="room-img">
                                </div>
                                <div class="col-8">
                                    <span class="fs12px">Room in home</span>
                                    <p class="subparagraph">Beautiful friendly family home</p>

                                </div>

                            </div>
                            <div class="my-4">
                                <hr>
                            </div>

                            <div class="d-flex justify-content-between align-items-center my-3">
                                <h6>${{$cart['product_price']}} X 5 nights</h6>
                                <h6>$556</h6>

                            </div>
                            <div class="d-flex justify-content-between align-items-center my-3">
                                <h6>Cleaning Fee</h6>
                                <h6>$0</h6>

                            </div>
                            <div class="d-flex justify-content-between align-items-center my-3">
                                <h6>Service Fee</h6>
                                <h6>$0</h6>

                            </div>
                            <div class="my-4">
                                <hr>
                            </div>
                            <div class="d-flex justify-content-between align-items-center my-3">
                                <h6><strong>Total before taxes</strong></h6>
                                <h6><strong>$556</strong></h6>

                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </section>



    {{-- eidt dates --}}
    <div class="modal fade" id="dateEdit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="d-flex">
                        <h5 class="card-title me-2">${{$cart['product_price']}}
                        </h5>
                        <span>night</span>
                    </div>
                    <div class="col-12">
                        <label for="start_date" class="mb-3"> Start Date</label>
                        <input type="date" name="start_date" id="start_date" class="form-control" value="{{$cart['start_date']}}">
                    </div>
                    <div class="col-12">
                        <label for="end_date" class="mb-3"> End Date</label>
                        <input type="date" name="end_date" id="end_date" class="form-control" value="{{$cart['end_date']}}">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
    {{-- eidt guests --}}
    <div class="modal fade" id="guestEdit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row justify-content-between align-items-center mt-3">
                        <div class="col-6">
                            <h6 class="card-title me-2">Adults
                            </h6>
                            <span>Age 13+</span>
                        </div>
                        <div class="col-6">
                            <div class="counter">
                                <span class="down" onClick='decreaseCount(event, this)'>-</span>
                                <input type="text" value="{{$cart['adults']}}">
                                <span class="up" onClick='increaseCount(event, this)'>+</span>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-between align-items-center my-3">
                        <div class="col-6">
                            <h6 class="card-title me-2">Children
                            </h6>
                            <span>Ages 2-12</span>
                        </div>
                        <div class="col-6">
                            <div class="counter">
                                <span class="down" onClick='decreaseCount(event, this)'>-</span>
                                <input type="text" value="{{$cart['childs']}}">
                                <span class="up" onClick='increaseCount(event, this)'>+</span>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-between align-items-center ">
                        <div class="col-6">
                            <h6 class="card-title me-2">Infants
                            </h6>
                            <span>Under 2</span>
                        </div>
                        <div class="col-6">
                            <div class="counter">
                                <span class="down" onClick='decreaseCount(event, this)'>-</span>
                                <input type="text" value="{{$cart['infants']}}">
                                <span class="up" onClick='increaseCount(event, this)'>+</span>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-between align-items-center my-3">
                        <div class="col-6">
                            <h6 class="card-title me-2">Pets
                            </h6>
                            <span><a href="" class="text-decoration-underline text-dark">Bringing
                                    a service email?</a></span>
                        </div>
                        <div class="col-6">
                            <div class="counter">
                                <span class="down" onClick='decreaseCount(event, this)'>-</span>
                                <input type="text" value="{{$cart['pets']}}">
                                <span class="up" onClick='increaseCount(event, this)'>+</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>


    @push('front_js')
        <script>
            function increaseCount(a, b) {
                var input = b.previousElementSibling;
                var value = parseInt(input.value, 10);
                value = isNaN(value) ? 0 : value;
                value++;
                input.value = value;
            }

            function decreaseCount(a, b) {
                var input = b.nextElementSibling;
                var value = parseInt(input.value, 10);
                if (value > 1) {
                    value = isNaN(value) ? 0 : value;
                    value--;
                    input.value = value;
                }
            }
        </script>
    @endpush
@endsection
