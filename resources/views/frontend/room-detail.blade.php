@extends('layouts.app')

@section('front_content')

    <section class="" style="margin-top:150px">
        <div class="container ">
            <div class="row g-4">
                <div class=" col-lg-6">
                    <img src="{{ asset($product->product_image) }}" alt="City Image"
                         class="product-image-detail h425">
                </div>

                <div class="col-lg-6">
                    <div class="row g-4">
                        <div class="col-lg-6">
                            <div class="row g-4">
                                @if(isset($product->ProductImages[0]))
                                    <div class="col-12">
                                        <img src="{{ asset($product->ProductImages[0]->image) }}" alt="City Image"
                                             class="product-image-detail">
                                    </div>
                                @endif
                                @if(isset($product->ProductImages[1]))
                                    <div class="col-12">
                                        <img src="{{ asset($product->ProductImages[1]->image) }}  " alt="City Image"
                                             class="product-image-detail">
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="row g-4">
                                @if(isset($product->ProductImages[2]))
                                    <div class="col-12">
                                        <img src="{{asset($product->ProductImages[2]->image)  }}  " alt="City Image"
                                             class="product-image-detail border-right-radius">
                                    </div>
                                @endif
                                @if(isset($product->ProductImages[3]))
                                    <div class="col-12">
                                        <img src="{{ asset($product->ProductImages[3]->image)  }}" alt="City Image"
                                             class="product-image-detail border-right-radius">
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <section class="my-4">
                <div class="row g-4">
                    <div class="col-lg-8">
                        <div class="d-flex justify-content-between align-items-center">
                            <div>
                                <strong class="text-capitalize" style="font-size: 20px">{{$product->short_description}}</strong>
                            </div>
                            <div>
                                <img src="{{ asset($product->product_image) }}  " alt="City Image"
                                     class="host-image">
                            </div>
                        </div>

                        <div class="row g-4 my-2">
                            @foreach($product->ProductFacilities as $fac)
                                <div class="col-lg-4 align-content-stretch">
                                    <div class="room-specification ">
                                        <span>
                                            {!! $fac->icon  !!}
                                        </span>
                                        <h6 class="text-capitalize">{{$fac->details}}</h6>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        @if(count($product->ProductFeatures) > 0)
                            <div class="my-4">
                                <hr>
                            </div>

                            <div>
                                @foreach($product->ProductFeatures as $feature)
                                    <div class="d-flex  w-75 my-3">
                                        <div>
                                            <div class="date">
                                                {!! $feature->icon !!}
                                            </div>
                                        </div>
                                        <div class="row ms-3">
                                            <div class="col-12 detail pe-4">
                                                <h6 class="text-capitalize">{{$feature->details}}</h6>
                                                <p>{{$feature->description}}</p>

                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="my-4">
                                <hr>
                            </div>
                        @endif
                        <div>
                            <h3>About this place</h3>
                            <p>{{$product->long_description}}</p>
                        </div>
                        <div class="my-4">
                            <hr>
                        </div>
                        <div>
                            <h3 class="mb-4">Meet your host</h3>
                            <div class="host-card">
                                <div>
                                    <div class="card mb-3  host-profile-card-inner" style="width: 80% !important;">
                                        <div class="row g-2">
                                            <div class="col-5">
                                                <img src="{{ asset('frontend/asset/images/city/c3.jpg') }}  "
                                                     alt="City Image"
                                                     class="">
                                            </div>
                                            <div class="col-7 d-flex align-items-center justify-content-center">
                                                <h4 class="text-center">
                                                    <strong>Chiara</strong>
                                                    <p class="text-center">Host</p>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
{{--                        <div>--}}
{{--                            <h3 class="mb-4">Where you'll sleep</h3>--}}
{{--                            <div class="col-6">--}}
{{--                                <img src="{{ asset('frontend/asset/images/city/c3.jpg') }}  " alt="City Image"--}}
{{--                                     class=" w-100 rounded-2" style="height: 300px">--}}
{{--                                <h5 class="card-title mt-3">Bedroom</h5>--}}
{{--                                <p class="card-text">1 double bed</p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="my-4">--}}
{{--                            <hr>--}}
{{--                        </div>--}}

                        <div class="row">
                            @foreach($product->ProductOffers as $offer)
                                <div class="col-lg-6 d-flex  w-75 my-3">
                                <div>
                                    <div class="date">
                                   {!! $offer->icon !!}
                                    </div>
                                </div>
                                <div class="row ms-3">
                                    <div class="col-12 detail pe-4">

                                        <h6>{{$offer->details}}</h6>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>

                    </div>
                    <div class="col-lg-4">
                        <div class="card shadow-sm rounded-2" style="width:100%">
                            <div class="card-body">
                                <div class="d-flex">
                                    <h5 class="card-title me-2">${{$product->price}}
                                    </h5>
                                    <span>night</span>
                                </div>
                                <form action="{{url('book-room')}}" method="POST">
                                    @csrf
                                    <input type="hidden" name="product_id" value="{{$product->id}}">
                                    <input type="hidden" name="vendor_id" value="{{$product->added_by}}">
                                    <input type="hidden" name="product_price" value="{{$product->price}}">
                                    <div class="border my-4 rounded">
                                        <div class="row">
                                            <div class="col-12 mb-3">
                                                <div class="p-2">
                                                    <label for="start_date">Check In</label>
                                                    <input type="date" name="start_date" id="start_date"
                                                           class="form-control border-0" required>
                                                </div>
                                            </div>
                                            <div class="col-12 mb-3">
                                                <div class="p-2">
                                                    <label for="end_date">Check Out</label>
                                                    <input type="date" name="end_date" id="end_date"
                                                           class="form-control border-0" required>
                                                </div>
                                            </div>
                                            <div class="col-12 ">
                                                <div class="border-top">
                                                    <div class="accordion accordion-flush" id="accordionFlushExample">
                                                        <div class="accordion-item">
                                                            <h2 class="accordion-header" id="flush-headingOne">
                                                                <button class="accordion-button" type="button"
                                                                        data-bs-toggle="collapse"
                                                                        data-bs-target="#flush-collapseOne"
                                                                        aria-expanded="false"
                                                                        aria-controls="flush-collapseOne">
                                                                    Guests
                                                                </button>
                                                            </h2>
                                                            <div id="flush-collapseOne" class="accordion-collapse collapse show"
                                                                 aria-labelledby="flush-headingOne"
                                                                 data-bs-parent="#accordionFlushExample">
                                                                <div class="accordion-body">
                                                                    <div
                                                                        class="row justify-content-between align-items-center mt-3">
                                                                        <div class="col-6">
                                                                            <h6 class="card-title me-2">Adults
                                                                            </h6>
                                                                            <span>Age 13+</span>
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <div class="counter">
                                                                                <span class="down"
                                                                                      onClick='decreaseCount(event, this)'>-</span>
                                                                                <input type="text" value="1" name="adults">
                                                                                <span class="up"
                                                                                      onClick='increaseCount(event, this)'>+</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div
                                                                        class="row justify-content-between align-items-center my-3">
                                                                        <div class="col-6">
                                                                            <h6 class="card-title me-2">Children
                                                                            </h6>
                                                                            <span>Ages 2-12</span>
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <div class="counter">
                                                                                <span class="down"
                                                                                      onClick='decreaseCount(event, this)'>-</span>
                                                                                <input type="text" value="0" name="childs">
                                                                                <span class="up"
                                                                                      onClick='increaseCount(event, this)'>+</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div
                                                                        class="row justify-content-between align-items-center ">
                                                                        <div class="col-6">
                                                                            <h6 class="card-title me-2">Infants
                                                                            </h6>
                                                                            <span>Under 2</span>
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <div class="counter">
                                                                                <span class="down"
                                                                                      onClick='decreaseCount(event, this)'>-</span>
                                                                                <input type="text" value="0" name="infants">
                                                                                <span class="up"
                                                                                      onClick='increaseCount(event, this)'>+</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div
                                                                        class="row justify-content-between align-items-center my-3">
                                                                        <div class="col-6">
                                                                            <h6 class="card-title me-2">Pets
                                                                            </h6>
    {{--                                                                        <span><a href=""--}}
    {{--                                                                                 class="text-decoration-underline text-dark">Bringing--}}
    {{--                                                                                a service email?</a></span>--}}
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <div class="counter">
                                                                                <span class="down"
                                                                                      onClick='decreaseCount(event, this)'>-</span>
                                                                                <input type="text" value="0" name="pets">
                                                                                <span class="up"
                                                                                      onClick='increaseCount(event, this)'>+</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <button class="btn  text-white w-100 p-2"
                                        style="background-color:var(--sky-blue);">Reserve</button>
                                </form>
                                <p class="text-center my-3">You won't be charged yet</p>
{{--                                <div class="d-flex justify-content-between align-items-center my-3">--}}
{{--                                    <h6>$118 X 5 nights</h6>--}}
{{--                                    <h6>$556</h6>--}}

{{--                                </div>--}}
{{--                                <div class="d-flex justify-content-between align-items-center my-3">--}}
{{--                                    <h6>Cleaning Fee</h6>--}}
{{--                                    <h6>$556</h6>--}}

{{--                                </div>--}}
{{--                                <div class="d-flex justify-content-between align-items-center my-3">--}}
{{--                                    <h6>Service Fee</h6>--}}
{{--                                    <h6>$556</h6>--}}

{{--                                </div>--}}
{{--                                <div class="my-4">--}}
{{--                                    <hr>--}}
{{--                                </div>--}}
{{--                                <div class="d-flex justify-content-between align-items-center my-3">--}}
{{--                                    <h6><strong>Total before taxes</strong></h6>--}}
{{--                                    <h6><strong>$556</strong></h6>--}}

{{--                                </div>--}}
                            </div>
                        </div>

                    </div>
                </div>
                <div class="my-4">
                    <hr>
                </div>
                <div>
                    <h3 class="mb-3">Things to know</h3>
                    <div class="row">
                        <div class="col-lg-4">
                            <h5 class="mb-3">House rules</h5>
                            <ul class="things-lists">
                                <li>Check-in after 4:00 PM</li>
                                <li>Checkout before 11:00 AM</li>
                                <li>2 guests maximum</li>
                            </ul>
                        </div>
                        <div class="col-lg-4">
                            <h5 class="mb-3">Safety & property</h5>
                            <ul class="things-lists">
                                <li>Carbon monoxide alarm</li>
                                <li>Smoke alarm</li>
                            </ul>
                        </div>
                        <div class="col-lg-4">
                            <h5 class="mb-3">Cancellation policy</h5>
                            <ul class="things-lists">
                                <li>Free cancellation before Jul 14.</li>
                                <li>Review the Host's full cancellation policy which applies even if you cancel for
                                    illness
                                    or disruptions caused by COVID-19.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="my-4">
                    <hr>
                </div>
            </section>
        </div>
    </section>



    @push('front_js')
        <script>
            function increaseCount(a, b) {
                var input = b.previousElementSibling;
                var value = parseInt(input.value, 10);
                value = isNaN(value) ? 0 : value;
                value++;
                input.value = value;
            }

            function decreaseCount(a, b) {
                var input = b.nextElementSibling;
                var value = parseInt(input.value, 10);
                if (value > 1) {
                    value = isNaN(value) ? 0 : value;
                    value--;
                    input.value = value;
                }
            }
        </script>
    @endpush
@endsection
