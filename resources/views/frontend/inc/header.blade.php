<header>
    <section class="topnav fixed-top">
        <div class="container">
            <div class="row">
                <div class=" d-flex justify-content-start align-items-center">
                    <div class="row">
                        <div class="d-md-none">
                            <div class="itemdiv ">
                                <img src="{{ asset('frontend/asset/logo/logo-sm.png') }}" alt=""
                        style="height:50px;max-width:100px" >
                            </div>
    
                        </div>
                        <div class="d-none d-md-flex align-items-center">
                            <div class="itemdiv">
                                <a href="https://mobile.twitter.com/login" target="_blank" class="navitem">
                                    <span><i class="fab fa-twitter"></i></span>
                                    <p class="d-none d-md-block  m-0">Twitter</p>
                                </a>
                            </div>
    
                            <div class="itemdiv">
                                <a href="https://www.facebook.com/" target="_blank" class="navitem">
                                    <span><i class="fab fa-facebook-square"></i></span>
                                    <p class="d-none d-md-block m-0">Facebook</p>
                                </a>
                            </div>
    
                            <div class="itemdiv">
                                <a href="https://www.instagram.com/" target="_blank" class="navitem">
                                    <span><i class="fab fa-instagram"></i></span>
                                    <p class="d-none d-md-block m-0">Instagram</p>
                                </a>
                            </div>
                        </div>
                       
                    </div>
                    <div class="ms-auto d-flex">
                        @if (is_null(auth()->user()))
                            <div class="itemdiv">
                                <a href="#" class="navitem" data-bs-toggle="modal" data-bs-target="#loginform">
                                    {{-- <span><i class="fas fa-heart"></i></span> --}}
                                    <p class="d-none d-md-block m-0">Login</p>
                                </a>
                            </div>

                            <div class="itemdiv">
                                <a href="#" class="navitem" data-bs-toggle="modal" data-bs-target="#signupform">
                                    <span><i class="fas fa-user"></i></span>
                                    <p class="d-none d-md-block m-0">Register</p>
                                </a>
                            </div>
                        @else
                            <div class="itemdiv">
                                <div class=" dropdown">
                                    <a class=" navitem text-white" href="#" role="button"
                                        data-bs-toggle="dropdown" aria-expanded="false">
                                        <span><i class="fas fa-user"></i></span>
                                        <p class=" m-0 ">Logout</p>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item" href="#">Go to Dashboard</a></li>
                                        <li><a class="dropdown-item" href="#">Logout</a></li>

                                    </ul>
                                </div>

                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <!-- Navbar Start Here -->
        {{-- <nav class="navbar navbar-expand-lg navbar-light"> --}}
        <div class="container-fluid px-5 bg-light shadow-sm">
            <div class="row align-items-center">
                <div class="d-none d-md-inline-block col-md-2">
                    <img src="{{ asset('frontend/asset/logo/logo-header.png') }}" alt=""
                        style="max-height:100px;max-width:200px" class='w-100'>

                </div>
                <div class="col-12 col-md-10">
                    <div class="swiper w-100 px-5">
                        <div class="swiper-wrapper">
                            @php
                                $categories = session()->get('categories');
                            @endphp
                            @foreach ($categories as $category)
                                <div class="swiper-slide " style="max-width: 100px">
                                    <a href="{{ url('/?category=') . $category->id }}"
                                        class="text-center  d-inline-flex align-items-center justify-content-center  text-nowrap active">
                                        <div class="d-block">
                                            <div>
                                                {!! $category->icon !!}
                                                {{--                                            <i class="fas fa-phone-square-alt"></i> --}}
                                            </div>
                                            <div>{{ $category->name }}</div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                </div>
            </div>
        </div>
        {{-- </nav> --}}

        <!-- Navbar End Here -->
    </section>

</header>

{{-- start login modal --}}
<div class="modal fade" id="loginform" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Login</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class=" col-lg-12">
                        <form class="authentication-from" method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="mb-3">
                                <input type="email" name="email" id="email" placeholder="Enter Email"
                                    class="col-12">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="my-3">
                                <input type="password" placeholder="Enter Password" class="col-12" id="password"
                                    name="password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            {{-- <div class="my-3 forget text-end">
                                <a href="forget.html" class="pe-2">Forget Password?</a>
                            </div> --}}
                            <div class="my-3">
                                <input type="submit" value="Login">
                            </div>
                            <div class="my-3 account">
                                <p>Don't have an Account? </p>
                                <a href="#" data-bs-target="#signupform" data-bs-toggle="modal"
                                    data-bs-dismiss="modal" aria-label="Close">Signin</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            {{-- <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div> --}}
        </div>
    </div>
</div>

{{-- end login form --}}

{{-- start signup form --}}
{{-- <div class="modal fade" id="signupform" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Sign Up</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class=" col-lg-12">
                        <form class="authentication-from" method="POST" action="{{ route('register') }}">
                            @csrf
                            <div class="row">
                                <div class="col-md-6 mt-3">
                                    <input type="text" placeholder="First Name" class="col-12" name="first_name"
                                        required />
                                </div>
                                <div class="col-md-6 my-3">
                                    <input type="text" placeholder="Last Name" class="col-12" name="last_name" />
                                </div>
                            </div>
                            <div class="mb-3">
                                <input type="email" placeholder="Enter Email" class="col-12" name="email"
                                    required />

                            </div>
                            <div class="my-3">
                                <input type="tel" id="phone" name="phone" class="col-12"
                                    placeholder="Enter Phone" required />
                            </div>
                            <div class="my-3">
                                <input type="password" placeholder="Enter Password" name="password" class="col-12"
                                    required />
                            </div>
                            <div class="my-3">
                                <input type="password" placeholder="Re-enter Password" name="password_confirmation"
                                    class="col-12" required />
                            </div>
                            <div class="my-3">
                                <select name="user_type" id="user_type" class="form-control col-12" required>
                                    <option value="">Select User Type</option>
                                    <option value="customer">Customer</option>
                                    <option value="vendor">Vendor</option>
                                </select>
                            </div>
                            <div class="my-3">
                                <input type="submit" value="Signin" />
                            </div>
                            <div class="my-3 account">
                                <p>Already have an Account?</p>
                                <a href="#" data-bs-target="#loginform" data-bs-toggle="modal"
                                    data-bs-dismiss="modal" aria-label="Close">Login</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div> --}}
{{-- end signup from --}}

{{-- @if (Session::has('errors') && session()->has('login_modal')) --}}
{{-- {{dd('here')}} --}}
{{--    @endif --}}


@push('front_js')

    @if (Session::has('errors') && session()->has('login_modal'))
        @if (session()->get('login_modal') == true)
            <script>
                let loginModal = new bootstrap.Modal(document.getElementById("loginform"));
                loginModal.show();
            </script>
        @endif
    @endif
    <!-- Signup Modal Error -->
    @if (Session::has('errors') && session()->has('register_modal'))
        @if (session()->has('register_modal') == true)
            <script>
                let signinModal = new bootstrap.Modal(
                    document.getElementById("signupform")
                );
                signinModal.show();
            </script>
        @endif
    @endif
@endpush
