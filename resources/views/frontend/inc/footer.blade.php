<footer class="py-5 ">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6 col-lg-3 fdiv1 my-4">
          <a href="index.html"><img src="{{ asset('frontend/asset/logo/logo-booking.svg') }}" alt="Logo" class="logo"></a>
          <p>Lorem ipsum dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
          <div class="contact">
            <span><i class="fas fa-phone-square-alt"></i></span>
            <span>
              <p>Call Agent</p>
              <p>123-456-789</p>
            </span>
          </div>
        </div>
        <div class="d-none d-lg-block col-lg-1"></div>
        <div class="col-md-6 col-lg-2 fdiv2 my-4">
          <h2 class="mb-5">Explore</h2>
          <ul>
            <li><a href="about.html">About Us</a></li>
            <li><a href="#">My Account</a></li>
            <li><a href="properties.html">Add Property</a></li>
            <li><a href="priceplan.html">Pricing Packages</a></li>
            <li><a href="contact.html">Contact Us</a></li>
          </ul>
        </div>
        <div class="d-none d-lg-block col-lg-1"></div>
        <div class="col-md-6 col-lg-2 fdiv3 my-4">
          <h2 class="mb-5">Account</h2>
          <ul>
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Members</a></li>
            <li><a href="agent.html">Our Agents</a></li>
            <li><a href="#">Our Agency</a></li>
            <li><a href="blog.html">Blog</a></li>
          </ul>
        </div>
        <div class="col-md-6 col-lg-3 my-4 fdiv4">
          <h2 class="mb-5">Newsletter</h2>
          <form>
            <div class="my-3">
              <input type="email" placeholder="Enter Emal Address" class="col-12">
            </div>
            <div class="my-3">
              <input type="submit" value="Subscribe" class="col-12 orange75">
            </div>
          </form>
          <p>625 Fulton Drive Fitchburg, MA 01420. UK</p>
          <p>support@csoftsystems.com</p>
        </div>
      </div>
    </div>
  </footer>
