<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>

<script>
        {{-- Swal --}}
    const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 5000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })
        document.addEventListener("DOMContentLoaded", function() {
            new WOW().init();
        });

        function animateElement(element) {
            // Add a class to trigger the animation
            element.classList.add('animated');

            // Remove the class after the animation ends
            setTimeout(function() {
                element.classList.remove('animated');
            }, 300); // Adjust the time to match the transition duration in CSS
        }
</script>


{{--@include('includes.flash_messages')--}}
<!-- Bootstrap JS FIle -->
<script src="{{ asset('frontend/asset/js/popper.min.js') }}"></script>
<script src="{{ asset('frontend/asset/js/bootstrap.min.js') }}"></script>
<!-- Jquery File -->
<script src="{{ asset('frontend/asset/jquery/jquery-3.6.0.min.js') }}"></script>
<script src="{{ asset('frontend/asset/jquery/swiper-bundle.min.js') }}"></script>

<script>
     var swiper = new Swiper('.swiper', {
        // slidesPerView: 3,
        slidesPerView: "auto",
        spaceBetween: 12,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        // breakpoints: {
        //     640: {
        //         slidesPerView: 4,
        //     },
        //     768: {
        //         slidesPerView: 5,
        //     },
        //     991: {
        //         slidesPerView: 6,
        //     },
        // },

    });
</script>
